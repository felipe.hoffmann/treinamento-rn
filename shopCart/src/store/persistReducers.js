﻿import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer } from 'redux-persist';

export default reducers => {
  const persistedReducer = persistReducer(
    {
      key: '@shopCart',
      storage: AsyncStorage,
      whitelist: ['auth', 'cart'],
    },
    reducers,
  );

  return persistedReducer;
};
