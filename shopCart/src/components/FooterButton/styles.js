﻿import styled from 'styled-components/native';

export const Touchable = styled.TouchableOpacity`
  margin-bottom: 15px;
`;

export const Container = styled.View`
  align-items: center;
  border-radius: 32px;
  background-color: #fc7422;
  opacity: ${props => (props.disabled ? 0.5 : 1)};

  margin: 10px;
  padding: 15px;
`;

export const TextButton = styled.Text`
  font-weight: bold;
  font-size: 13px;
  color: #fff;
`;
