﻿import React from 'react';
import { WebView } from 'react-native-webview';
import { useRoute } from '@react-navigation/native';

const Repository = () => {
  const routes = useRoute();

  const { repository } = routes.params;

  return <WebView source={{ uri: repository.html_url }} style={{ flex: 1 }} />;
};

export default Repository;
