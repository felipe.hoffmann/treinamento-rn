﻿import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import FirstPage from '../pages/FirstPage';
import SecondPage from '../pages/SecondPage';
import FinalPage from '../pages/FinalPage';

const Stack = createStackNavigator();

const Routes = () => (
  <NavigationContainer>
    <Stack.Navigator>

      <Stack.Screen
        name="First"
        component={FirstPage}
        options={() => ({
          headerStyle: { backgroundColor: '#fc7422' },
          headerTintColor: '#fff',
        })}
      />

      <Stack.Screen
        name="Second"
        component={SecondPage}
      />

      <Stack.Screen
        name="Final"
        component={FinalPage}
      />

    </Stack.Navigator>
  </NavigationContainer>
)


export default Routes;