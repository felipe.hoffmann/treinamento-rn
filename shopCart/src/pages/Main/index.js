﻿import React, { useState, useEffect } from 'react';
import { FlatList, Alert, View, Text } from 'react-native';
import api from '../../services/api';
import { useNavigation, useIsFocused } from '@react-navigation/native';

import FooterButton from '../../components/FooterButton';

import { Container, ContainerEmptyList, TextEmptyList } from './styles';

const Main = () => {
  const [messages, setMessages] = useState([]);
  const navigation = useNavigation();
  const isFocused = useIsFocused();

  useEffect(() => {
    async function loadMessages() {
      try {
        const { data } = await api.get('/api/mensagem');
        setMessages(data);
      } catch (err) {
        Alert.alert('Ops', err);
      }
    }

    if (isFocused) {
      loadMessages();
    }
  }, [isFocused]);

  const EmptyList = () => (
    <ContainerEmptyList>
      <TextEmptyList>Sua lista está vazia</TextEmptyList>
    </ContainerEmptyList>
  );

  const handleNewItem = () => {
    navigation.navigate('Cart');
  };

  return (
    <Container>
      <FlatList
        data={messages}
        keyExtractor={() => Math.random().toString()}
        ListEmptyComponent={<EmptyList />}
        ListFooterComponent={
          <FooterButton text="CRIAR" onPress={handleNewItem} />
        }
        renderItem={({ item }) => (
          <View style={{ padding: 10 }}>
            <Text>{item.msg}</Text>
          </View>
        )}
      />
    </Container>
  );
};

export default Main;
