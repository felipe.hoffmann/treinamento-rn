﻿import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';

import PlantSelect from '../pages/PlantSelect';
import MyPlants from '../pages/MyPlants';

import colors from '../../styles/colors';

const Tab = createBottomTabNavigator();

const TabBarIcon = ({ icon, size, color }) => (
  <MaterialIcons name={icon} size={size} color={color} />
);
TabBarIcon.propTypes = {
  icon: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};

const TabRoutes = () => (
  <Tab.Navigator
    tabBarOptions={{
      activeTintColor: colors.green,
      inactiveTintColor: colors.heading,
      labelPosition: 'beside-icon',
      style: {
        paddingVertical: Platform.OS === 'ios' ? 20 : 0,
        height: 88,
      },
    }}
  >
    <Tab.Screen
      name="Nova planta"
      component={PlantSelect}
      options={{
        tabBarIcon: ({ size, color }) => (
          <TabBarIcon icon="add-circle-outline" size={size} color={color} />
        ),
      }}
    />

    <Tab.Screen
      name="Minhas plantinhas"
      component={MyPlants}
      options={{
        tabBarIcon: ({ size, color }) => (
          <TabBarIcon icon="format-list-bulleted" size={size} color={color} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default TabRoutes;
