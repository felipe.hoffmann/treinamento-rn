﻿import * as Sentry from '@sentry/react-native';
import { all, takeLatest, call, put } from 'redux-saga/effects';
import api from '../../../services/api';
import { signInFailure, signInSuccess } from './actions';

export function* signIn({ payload }) {
  const { user, pass } = payload;

  const ws = '/login';

  const body = {
    user,
    pass,
  };

  try {
    const response = yield call(api.post, ws, body);

    const { token } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(signInSuccess({ user, token }));
  } catch (error) {
    Sentry.captureException(error);
    yield put(signInFailure());
  }
}

export function* setToken({ payload }) {
  if (!payload) {
    return;
  }

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export default all([
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('persist/REHYDRATE', setToken),
]);
