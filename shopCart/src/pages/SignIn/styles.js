﻿import styled from 'styled-components/native';
import { Platform, Dimensions } from 'react-native';

export const Container = styled.View`
  flex: 1;
  background-color: #fc7422;
`;

export const KeyboardView = styled.KeyboardAvoidingView.attrs({
  enabled: Platform.OS === 'ios',
  behavior: 'padding',
  keyboardVerticalOffset: Dimensions.KEYBOARD_OFFSET,
})`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
`;

export const Logo = styled.Image.attrs({
  resizeMode: 'contain',
})`
  align-self: center;
  width: 90%;
  height: 200px;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: 'rgba(255,255,255, 0.8)',
})`
  padding: 10px 15px;
  background: rgba(0, 0, 0, 0.1);
  align-self: stretch;
  margin-top: 10px;
  border-radius: 4px;
  color: #fff;
  font-size: 15px;
  width: 100%;
`;

export const ContainerPassword = styled.View`
  flex-direction: row;
  padding-bottom: 10px;
  justify-content: flex-end;
`;

export const IconButton = styled.TouchableOpacity`
  position: absolute;
  align-self: center;
  padding-right: 10px;
`;

export const Button = styled.TouchableOpacity`
  padding: 15px;
  border-radius: 5px;
  background: #fff;
  align-self: stretch;
  margin-top: 25px;
`;

export const ButtonText = styled.Text`
  color: #fc7422;
  text-align: center;
  font-size: 16px;
`;
