﻿# Configuração do ambiente
## VSCode
<h3 id="instalacao-vscode">Instalação</h3>

Baixar e instalar o VSCode a partir [deste link](https://code.visualstudio.com/).

<h3 id="extensoes-vscode">Extensões</h3>

Instalar as extensões abaixo:

![](screenshots/extensions01.png)
![](screenshots/extensions02.png)

<h3 id="fonte-vscode">Fonte Fira Code</h3>

Baixar e instalar a fonte Fira Code: [link](https://github.com/tonsky/FiraCode/releases)

## NodeJS
<h3 id="nvm">NVM</h3>

Instalar o NVM (Node Version Manager), pois com ele é possível instalar e gerenciar de forma mais prática as versões do Node.
Faça a instalação seguindo [este link](https://github.com/nvm-sh/nvm).

<h3 id="instalacao-node">Instalação</h3>

Verificar a versão LTS no site do [NodeJS](https://nodejs.org/en/download/).
- Exemplo: v14.16

No terminal, executar o comando para a instalar:
```
nvm install 14.16
```

Para verificar se instalou com sucesso, rodar o comando:
```
node -v
```

## Yarn
Yarn é um gerenciador de pacotes.

<h3 id="instalacao-yarn">Instalação</h3>
Instalar o Yarn de forma global com o seguinte comando:

```
npm install --global yarn
```

## Expo CLI
Link do [Expo](https://expo.io/).

Link da [Documentação](https://docs.expo.io/).

<h3 id="instalacao-expo">Instalação</h3>

```
npm install --global expo-cli
```

<h3 id="instalacao-expo">Upgrade SDK</h3>

```
expo upgrade
```

## Emulador Android

### JDK
1 - Acessar o link ```https://adoptopenjdk.net/``` e baixar o JDK 11 com HotSpot.
2 - Extrair na pasta ```/opt``` e renomear para
```
jdk-11
```

Dar permissão com
```
sudo chown felipe chown -R
```

3 - Setar a versão corrente com os comandos abaixo:
```
sudo update-alternatives --install /usr/bin/java java /opt/jdk-11/bin/java 1
```

```
sudo update-alternatives --install /usr/bin/javac javac /opt/jdk-11/bin/javac 1
```


<h3 id="instalacao-android-studio">Android Studio</h3>

Utilizamos o emulador disponibilizado pelo Android Studio. Por isso, precisamos antes instalá-lo.

Faça o download pelo site oficial do [Android Studio](https://developer.android.com/studio).

<h3 id="configuracao-emulador-android">Configuração Emulador</h3>

1. Ao abrir o Android Studio, clicar em `Configure > AVD Manager`.

2. Clicar no botão `+ Create Virtual Device...`.

3. Escolha o o modelo `Pixel 4` e clique em `Next`.

4. Clique em `Download` na versão do Android desejada e efetue a instalação.

<h3 id="emulador-pelo-terminal">Abrindo emulador pelo terminal</h3>

Antes de tudo, precisamos configurar as variáveis de ambiente da SDK do Android.
Adicione no final do arquivo `.bashrc`:

```
export PATH="${HOME}/Library/Android/Sdk/tools:${HOME}/Library/Android/Sdk/platform-tools:${PATH}"
```

Listando os emuladores disponíveis:
```
emulator -list-avds
```

Executando um emulador:
```
emulator -avd Pixel_4_API_30
```

Nota: Verificar se o path apresentado acima é o mesmo que foi instalado em sua máquina. 
Nota: Se ao executar o comando de abrir o emulador (emulator) ocorrer algum erro, mude o export da pasta tools para a pasta emulator, e tente novamente.

## Ferramentas

### Insomnia

Ferramenta para testes de requisições à API
[Download](https://insomnia.rest/)

Importar o arquivo ```insomnia/insomnia.json``` para dentro do seu insomnia.

### Reactotron
É uma excelente ferramenta de Debug, podendo visualizar estados do Redux, requisições à API e diversas outras coisas.

Instalar conforme a plataforma a partir deste link: https://github.com/infinitered/reactotron/releases