﻿export function addToCart({ pedido }) {
  return {
    type: '@cart/ADD_TO_CART',
    payload: { pedido },
  };
}

export function checkoutRequest({ pedidos }) {
  return {
    type: '@cart/CHECKOUT_REQUEST',
    payload: { pedidos },
  };
}

export function checkoutSuccess() {
  return {
    type: '@cart/CHECKOUT_SUCCESS',
  };
}

export function checkoutFailure() {
  return {
    type: '@cart/CHECKOUT_FAILURE',
  };
}
