﻿import React, { useState } from "react";
import {
  Text, 
  TextInput, 
  View, 
  StyleSheet, 
  SafeAreaView, 
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";

import { useNavigation } from '@react-navigation/native';
import Button from '../../components/Button';

export default function FirstPage() {
  const navigation = useNavigation();

  const [name, setName] = useState('');

  function handleNext() {
    navigation.navigate("Second", { name });
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView 
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'heigth'}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.content}>

            <Text style={styles.text}>Digite seu nome</Text>

            <TextInput 
              style={styles.input}
              onChangeText={setName}
              placeholder='João da Silva'/> 

            <View style={styles.footer}>
              <Button
                onPressButton={handleNext}
                title='Continuar'
              />
            </View>

          </View>
        </TouchableWithoutFeedback>

      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: '100%',
  },
  content: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 20,
  },
  input: {
    color: '#333',
    fontSize: 16,
    borderBottomWidth: 1,
    borderColor: '#333',
    width: '100%',
    padding: 10,
    textAlign: 'center',
    marginTop: 50,
  },
  footer: {
    marginTop: 40,
    width: '100%',
  },
  button: {
    backgroundColor: '#fc7422',
    height: 56,
    justifyContent: 'center',
    borderRadius: 16,
    alignItems: 'center',

    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
    elevation: 15
  },
  textButton: {
    color: '#fff',
    fontSize: 16,
  }
})