﻿import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import { SvgFromUri } from 'react-native-svg';
import PropTypes from 'prop-types';

import colors from '../../styles/colors';
import fonts from '../../styles/fonts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxWidth: '45%',
    backgroundColor: colors.shape,
    borderRadius: 20,
    paddingVertical: 10,
    alignItems: 'center',
    margin: 10,
  },
  text: {
    color: colors.green_dark,
    fontFamily: fonts.heading,
    marginVertical: 16,
  },
});

const PlantCardPrimary = ({ data, ...rest }) => (
  <RectButton style={styles.container} {...rest}>
    <SvgFromUri uri={data.photo} width={70} height={70} />
    <Text style={styles.text}>{data.name}</Text>
  </RectButton>
);

PlantCardPrimary.propTypes = {
  data: PropTypes.shape({
    photo: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
};

export default PlantCardPrimary;
