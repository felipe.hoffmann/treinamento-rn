﻿import React from 'react';
import { View, FlatList, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { checkoutRequest } from '~/store/modules/cart/actions';
import { useNavigation } from '@react-navigation/native';

import FooterButton from '~/components/FooterButton';

import { Container } from './styles';

const Checkout = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const { pedidos, loading } = useSelector(state => state.cart);

  const handleFinish = () => {
    dispatch(checkoutRequest({ pedidos }));
    navigation.popToTop();
  };

  return (
    <Container>
      <FlatList
        data={pedidos}
        keyExtractor={() => Math.random().toString()}
        renderItem={({ item }) => (
          <View style={{ padding: 10 }}>
            <Text>{item}</Text>
          </View>
        )}
      />

      <FooterButton text="FINALIZAR" onPress={handleFinish} loading={loading} />
    </Container>
  );
};

export default Checkout;
