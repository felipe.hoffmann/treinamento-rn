﻿import React, { useState } from 'react';
import { TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { addToCart } from '../../store/modules/cart/actions';

import {
  Container,
  TextTitle,
  ContainerAmountControl,
  TextItem,
  ButtonAddToCart,
  CheckoutButton,
  ContainerBtnCheckout,
  ContainerIconBtnCheckout,
  IconQtdBtnCheckout,
  TextQtdCheckout,
} from './styles';

const Cart = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [qtdChapeu, setQtdChapeu] = useState(0);
  const [qtdCamiseta, setQtdCamiseta] = useState(0);
  const [qtdSapato, setQtdSapato] = useState(0);

  const { user } = useSelector(state => state.auth);
  const { pedidos } = useSelector(state => state.cart);

  const totalAmount = pedidos.length;

  const handleAdd = () => {
    const pedido = `Usuario ${user} comprou: ${qtdChapeu} chapéus, ${qtdCamiseta} camisetas e ${qtdSapato} sapatos`;

    dispatch(addToCart({ pedido }));

    setQtdChapeu(0);
    setQtdCamiseta(0);
    setQtdSapato(0);
  };

  const handleCheckout = () => {
    navigation.navigate('Checkout');
  };

  const AmountControl = ({ item, value, handlePlus, handleMinus }) => (
    <ContainerAmountControl>
      <TouchableOpacity onPress={handleMinus}>
        <Icon name="remove-circle" size={32} color="#fc7422" />
      </TouchableOpacity>

      <Text style={{ padding: 20, fontSize: 20 }}>{value}</Text>

      <TouchableOpacity onPress={handlePlus}>
        <Icon name="add-circle" size={32} color="#fc7422" />
      </TouchableOpacity>

      <TextItem>{item}</TextItem>
    </ContainerAmountControl>
  );

  const AddToCart = () => (
    <ButtonAddToCart onPress={handleAdd}>
      <Icon name="add" size={32} color="#fff" />
    </ButtonAddToCart>
  );

  const Checkout = () => (
    <CheckoutButton disabled={totalAmount === 0} onPress={handleCheckout}>
      <ContainerBtnCheckout>
        <ContainerIconBtnCheckout>
          <Icon name="shopping-cart" size={32} color="#fff" />
        </ContainerIconBtnCheckout>

        <IconQtdBtnCheckout>
          <TextQtdCheckout>{totalAmount}</TextQtdCheckout>
        </IconQtdBtnCheckout>
      </ContainerBtnCheckout>
    </CheckoutButton>
  );

  return (
    <Container>
      <TextTitle>Escolha os itens do seu carrinho</TextTitle>

      <AmountControl
        item="Chapéu"
        value={qtdChapeu}
        handlePlus={() => setQtdChapeu(oldValue => oldValue + 1)}
        handleMinus={() =>
          setQtdChapeu(oldValue => (oldValue > 0 ? oldValue - 1 : oldValue))
        }
      />

      <AmountControl
        item="Camiseta"
        value={qtdCamiseta}
        handlePlus={() => setQtdCamiseta(oldValue => oldValue + 1)}
        handleMinus={() =>
          setQtdCamiseta(oldValue => (oldValue > 0 ? oldValue - 1 : oldValue))
        }
      />

      <AmountControl
        item="Sapato"
        value={qtdSapato}
        handlePlus={() => setQtdSapato(oldValue => oldValue + 1)}
        handleMinus={() =>
          setQtdSapato(oldValue => (oldValue > 0 ? oldValue - 1 : oldValue))
        }
      />

      <AddToCart />

      <Checkout />
    </Container>
  );
};

export default Cart;
