﻿import React from 'react';
import { Alert } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import { useSelector, useDispatch } from 'react-redux';

import { signOut } from '../store/modules/auth/actions';

import SignIn from '../pages/SignIn';
import Main from '../pages/Main';
import Cart from '../pages/Cart';
import Checkout from '../pages/Checkout';

import SignOutButton from '../components/SignOutButton';

const Stack = createStackNavigator();

const MyStack = () => {
  const { signed } = useSelector(state => state.auth);
  const dispatch = useDispatch();

  return (
    <Stack.Navigator>
      {!signed ? (
        <Stack.Screen
          name="SignIn"
          component={SignIn}
          options={() => ({
            headerTitle: '',
            headerTransparent: true,
          })}
        />
      ) : (
        <>
          <Stack.Screen
            name="Main"
            component={Main}
            options={() => ({
              headerTitle: 'Home',
              headerTitleAlign: 'center',
              headerTintColor: '#fff',
              headerStyle: {
                backgroundColor: '#fc7422',
              },
              headerRight: () => (
                <SignOutButton
                  onPress={() => {
                    Alert.alert('Sair', 'Deseja realmente sair?', [
                      {
                        text: 'Não',
                        style: 'cancel',
                      },
                      {
                        text: 'Sim',
                        onPress: () => dispatch(signOut()),
                      },
                    ]);
                  }}
                />
              ),
            })}
          />
          <Stack.Screen
            name="Cart"
            component={Cart}
            options={() => ({
              headerTitle: 'Carrinho de compras',
              headerTitleAlign: 'center',
              headerTintColor: '#fff',
              headerStyle: {
                backgroundColor: '#fc7422',
              },
            })}
          />
          <Stack.Screen
            name="Checkout"
            component={Checkout}
            options={() => ({
              headerTitle: 'Checkout',
              headerTitleAlign: 'center',
              headerTintColor: '#fff',
              headerStyle: {
                backgroundColor: '#fc7422',
              },
            })}
          />
        </>
      )}
    </Stack.Navigator>
  );
};

export default MyStack;
