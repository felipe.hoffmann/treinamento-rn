﻿import React, { useState } from "react";
import {
  Text, 
  TextInput, 
  View, 
  StyleSheet, 
  SafeAreaView, 
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";

import { useNavigation, useRoute } from '@react-navigation/native';
import Button from '../../components/Button';

export default function SecondPage() {
  const navigation = useNavigation();
  const routes = useRoute();

  const { name } = routes.params;

  const [age, setAge] = useState();

  function handleNext() {
    navigation.navigate("Final", { name, age })
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView 
        style={styles.container}
        behavior={Platform.OS === 'ios' ? 'padding' : 'heigth'}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.content}>

            <Text style={styles.text}>Digite sua idade</Text>

            <TextInput 
              style={styles.input} 
              onChangeText={setAge}
              placeholder='25'/> 

            <View style={styles.footer}>
              <Button
                onPressButton={handleNext}
                title='Finalizar'
              />
            </View>

          </View>
        </TouchableWithoutFeedback>

      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: '100%',
  },
  content: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 20,
  },
  input: {
    color: '#333',
    fontSize: 16,
    borderBottomWidth: 1,
    borderColor: '#333',
    width: '100%',
    padding: 10,
    textAlign: 'center',
    marginTop: 50,
  },
  footer: {
    marginTop: 40,
    width: '100%',
  },
})