﻿import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
`;

export const ContainerEmptyList = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: 50px;
`;

export const TextEmptyList = styled.Text`
  color: #333;
`;
