﻿import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from '../pages/Main';
import User from '../pages/User';
import Repository from '../pages/Repository';

const Stack = createStackNavigator();

const Routes = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="Main"
        component={Main}
        options={() => ({
          headerStyle: { backgroundColor: '#fc7422' },
          headerTitle: 'Usuários',
          headerTitleAlign: 'center',
          headerTintColor: '#fff',
        })}
      />

      <Stack.Screen
        name="User"
        component={User}
        options={({ route }) => ({
          headerStyle: { backgroundColor: '#fc7422' },
          headerTitle: route.params.user.name,
          headerTitleAlign: 'center',
          headerTintColor: '#fff',
        })}
      />

      <Stack.Screen
        name="Repository"
        component={Repository}
        options={({ route }) => ({
          headerStyle: { backgroundColor: '#fc7422' },
          headerTitle: route.params.repository.name,
          headerTitleAlign: 'center',
          headerTintColor: '#fff',
        })}
      />
    </Stack.Navigator>
  </NavigationContainer>
);

export default Routes;
