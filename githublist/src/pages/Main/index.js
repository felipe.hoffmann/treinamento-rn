﻿import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { ActivityIndicator, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';
import api from '../../services/api';

import {
  Container,
  Form,
  Input,
  SubmitButton,
  List,
  User,
  Avatar,
  Name,
  Bio,
  ProfileButton,
  ProfileButtonText,
} from './styles';

const Main = () => {
  const navigation = useNavigation();

  const [newUser, setNewUser] = useState('');
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function loadUsers() {
      const usersStorage = await AsyncStorage.getItem('users');

      if (usersStorage) {
        setUsers(JSON.parse(usersStorage));
      }
    }

    loadUsers();
  }, []);

  useEffect(() => {
    if (users.length > 0) {
      AsyncStorage.setItem('users', JSON.stringify(users));
    }
  }, [users]);

  async function handleAddUser() {
    setLoading(true);

    try {
      const response = await api.get(`/users/${newUser}`);

      const { name, login, bio, avatar_url: avatar } = response.data;

      const data = {
        name,
        login,
        bio,
        avatar,
      };

      setUsers([...users, data]);
      setNewUser('');
    } catch (err) {
      Alert.alert('Oooops', `Ocorreu um erro: ${err}`);
    } finally {
      setLoading(false);
    }
  }

  const handleNavigate = user => {
    navigation.navigate('User', { user });
  };

  return (
    <Container>
      <Form>
        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Adicionar usuário"
          returnKeyType="send"
          onSubmitEditing={handleAddUser}
          value={newUser}
          onChangeText={setNewUser}
        />
        <SubmitButton loading={loading} onPress={handleAddUser}>
          {loading ? (
            <ActivityIndicator color="#fff" />
          ) : (
            <Icon name="add" size={20} color="#fff" />
          )}
        </SubmitButton>
      </Form>

      <List
        data={users}
        keyExtractor={item => item.login}
        renderItem={({ item }) => (
          <User>
            <Avatar source={{ uri: item.avatar }} />
            <Name>{item.name}</Name>
            <Bio>{item.bio}</Bio>

            <ProfileButton onPress={() => handleNavigate(item)}>
              <ProfileButtonText>Ver perfil</ProfileButtonText>
            </ProfileButton>
          </User>
        )}
      />
    </Container>
  );
};

export default Main;
