﻿import React from 'react';
import { 
  Text, 
  SafeAreaView,
  StyleSheet,
} from 'react-native';

import { useRoute } from '@react-navigation/native';

export default function FinalPage() {
  const routes = useRoute();

  const { name, age } = routes.params;

  return (
    <SafeAreaView style={styles.container}>
      
      <Text
        style={styles.textName}
      >
        Nome: {name}
      </Text>
      
      <Text
        style={styles.textAge}
      >
        Idade: {age}
      </Text>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: '100%',
  },
  textName: {
    color: '#333',
    fontSize: 20,
  },
  textAge: {
    color: '#333',
    fontSize: 16,
  }
});