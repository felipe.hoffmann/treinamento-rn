﻿import React from 'react';

import { Touchable, IconSignOut } from './styles';

const SignOutButton = ({ onPress }) => {
  return (
    <Touchable onPress={onPress}>
      <IconSignOut />
    </Touchable>
  );
};

export default SignOutButton;
