﻿export function signInRequest({ user, pass }) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { user, pass },
  };
}

export function signInFailure() {
  return {
    type: '@auth/SIGN_IN_FAILURE',
  };
}

export function signInSuccess({ user, token }) {
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { user, token },
  };
}

export function signOut() {
  return {
    type: '@auth/SIGN_OUT',
  };
}
