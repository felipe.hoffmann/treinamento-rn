﻿import React, { useState, useRef } from 'react';
import {
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { signInRequest } from '../../store/modules/auth/actions';
import { useDispatch, useSelector } from 'react-redux';

import {
  Container,
  Input,
  Button,
  ButtonText,
  Logo,
  KeyboardView,
  IconButton,
  ContainerPassword,
} from './styles';

import logo from '../../assets/logo.png';

const SignIn = () => {
  const passwordRef = useRef();
  const dispatch = useDispatch();
  const { loading } = useSelector(state => state.auth);

  const [user, setUser] = useState('');
  const [password, setPassword] = useState();

  const [passwordVisibility, setPasswordVisibility] = useState(false);
  const [visibilityIcon, setVisibilityIcon] = useState('visibility');

  const handleLogin = () => {
    if (user && password) {
      dispatch(signInRequest({ user, pass: password }));
    } else {
      Alert.alert('Atenção', 'Usuário e senha são obrigatórios');
    }
  };

  const handlePasswordVisibility = () => {
    if (passwordVisibility) {
      setPasswordVisibility(false);
      setVisibilityIcon('visibility');
    } else {
      setPasswordVisibility(true);
      setVisibilityIcon('visibility-off');
    }
  };

  return (
    <Container>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <KeyboardView>
          <Logo source={logo} />

          <Input
            placeholder="Usuário"
            autoCapitalize="none"
            autoCorrect={false}
            returnKeyType="next"
            blurOnSubmit={false}
            onChangeText={setUser}
            value={user}
            onSubmitEditing={() => passwordRef.current.focus()}
          />

          <ContainerPassword>
            <Input
              ref={passwordRef}
              placeholder="Senha"
              autoCapitalize="none"
              autoCorrect={false}
              returnKeyType="send"
              secureTextEntry={!passwordVisibility}
              onSubmitEditing={handleLogin}
              onChangeText={setPassword}
              enabledReturnKeyAutomatically
            />

            <IconButton onPress={handlePasswordVisibility}>
              <Icon name={visibilityIcon} color="#fff" size={35} />
            </IconButton>
          </ContainerPassword>

          <Button onPress={handleLogin}>
            {loading ? (
              <ActivityIndicator size="small" color="#fc7422" />
            ) : (
              <ButtonText>Login</ButtonText>
            )}
          </Button>
        </KeyboardView>
      </TouchableWithoutFeedback>
    </Container>
  );
};

export default SignIn;
