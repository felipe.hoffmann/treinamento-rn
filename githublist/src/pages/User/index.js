﻿import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import api from '../../services/api';
import {
  Container,
  Header,
  Avatar,
  Name,
  Bio,
  Stars,
  Title,
  Starred,
  OwnerAvatar,
  Info,
  Author,
  Loading,
} from './styles';

const User = () => {
  const routes = useRoute();
  const navigation = useNavigation();

  const { user } = routes.params;

  const [stars, setStars] = useState([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);

  const loadStarred = async (pageNumber = 1) => {
    setRefreshing(true);
    const response = await api.get(`users/${user.login}/starred`, {
      params: { page: pageNumber },
    });

    setStars(pageNumber >= 2 ? [...stars, ...response.data] : response.data);
    setPage(pageNumber);
    setLoading(false);
    setRefreshing(false);
  };

  useEffect(() => {
    loadStarred();
  }, []);

  const loadMore = () => {
    const nextPage = page + 1;

    loadStarred(nextPage);
  };

  const refreshList = () => {
    setRefreshing(true);
    setStars([]);
    loadStarred();
  };

  const handleNavigateToRepository = repository => {
    navigation.navigate('Repository', { repository });
  };

  return (
    <Container>
      <Header>
        <Avatar source={{ uri: user.avatar }} />
        <Name>{user.name}</Name>
        <Bio>{user.bio}</Bio>
      </Header>

      {/* {loading ? (
        <Loading color="#fc7422" />
      ) : ( */}
      <Stars
        data={stars}
        keyExtractor={star => String(star.id)}
        onEndReached={loadMore} // Função que vai carregar mais itens. Disparada qd chegar no fim
        onEndReachedThreshold={0.2} // Vai disparar quando chegar em 20% do fim
        onRefresh={refreshList} // Função disparada quando o usuário arrasta a lista para baixo
        refreshing={refreshing} // Variável que armazena um estado true/false que representa se a lista está atualizando
        renderItem={({ item }) => (
          <Starred>
            <TouchableOpacity
              onPress={() => handleNavigateToRepository(item)}
              style={{ flexDirection: 'row' }}
            >
              <OwnerAvatar source={{ uri: item.owner.avatar_url }} />
              <Info>
                <Title>{item.name}</Title>
                <Author>{item.owner.login}</Author>
              </Info>
            </TouchableOpacity>
          </Starred>
        )}
      />
      {/* )} */}
    </Container>
  );
};

export default User;
