﻿import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Text,
  FlatList,
  ActivityIndicator,
  Alert,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import api from '../services/api';

import Header from '../components/Header';
import EnvironmentButton from '../components/EnvironmentButton';
import PlantCardPrimary from '../components/PlantCardPrimary';
import Load from '../components/Load';

import colors from '../../styles/colors';
import fonts from '../../styles/fonts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
  },
  header: {
    paddingHorizontal: 30,
  },
  title: {
    fontSize: 17,
    color: colors.heading,
    fontFamily: fonts.heading,
    lineHeight: 20,
    marginTop: 15,
  },
  subtitle: {
    fontSize: 17,
    color: colors.heading,
    fontFamily: fonts.text,
    lineHeight: 20,
  },
  environmentList: {
    height: 40,
    justifyContent: 'center',
    paddingBottom: 5,
    paddingLeft: 32,
    marginVertical: 32,
  },
  plant: {
    flex: 1,
    paddingHorizontal: 32,
    justifyContent: 'center',
  },
});

const PlantSelect = () => {
  const navigation = useNavigation();

  const [loading, setLoading] = useState(true);

  const [environments, setEnvironments] = useState([]);
  const [environmentSelected, setEnvironmentSelected] = useState('all');

  const [plants, setPlants] = useState([]);
  const [filteredPlants, setFilteredPlants] = useState([]);

  const [page, setPage] = useState(1);
  const [loadingMore, setLoadingMore] = useState(false);

  async function fetchPlants() {
    try {
      const { data } = await api.get(
        `plants?_sort=name&_order=asc&_page=${page}&_limit=8`,
      );

      if (!data) {
        setLoading(false);
        setLoadingMore(false);
        return;
      }

      if (page > 1) {
        setPlants(oldValue => [...oldValue, ...data]);
        setFilteredPlants(oldValue => [...oldValue, ...data]);
      } else {
        setPlants(data);
        setFilteredPlants(data);
      }
    } catch {
      Alert.alert('Oooops...', 'Houve um erro');
    }
    setLoading(false);
    setLoadingMore(false);
  }

  useEffect(() => {
    fetchPlants();
  }, []);

  function handleFetchMore(distance) {
    if (distance < 1) return;

    setLoadingMore(true);
    setPage(oldValue => oldValue + 1);
    fetchPlants();
  }

  useEffect(() => {
    async function fetchEnvironments() {
      try {
        const { data } = await api.get(
          'plants_environments?_sort=title&_order=asc',
        );

        setEnvironments([{ key: 'all', title: 'Todos' }, ...data]);
      } catch {
        Alert.alert('Oooops...', 'Houve um erro');
      }
    }

    fetchEnvironments();
  }, []);

  function handleEnvironmentSelected(environment) {
    setEnvironmentSelected(environment);

    if (environment === 'all') return setFilteredPlants(plants);

    const filtered = plants.filter(plant =>
      plant.environments.includes(environment),
    );

    return setFilteredPlants(filtered);
  }

  function handlePlantSelect(plant) {
    navigation.navigate('PlantSave', { plant });
  }

  if (loading) return <Load />;

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Header />

        <Text style={styles.title}>Em qual ambiente</Text>
        <Text style={styles.subtitle}>você quer colocar sua planta</Text>
      </View>

      <View>
        <FlatList
          data={environments}
          keyExtractor={item => item.key}
          renderItem={({ item }) => (
            <EnvironmentButton
              title={item.title}
              active={item.key === environmentSelected}
              onPress={() => handleEnvironmentSelected(item.key)}
            />
          )}
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.environmentList}
        />
      </View>

      <View style={styles.plant}>
        <FlatList
          data={filteredPlants}
          keyExtractor={item => String(item.id)}
          renderItem={({ item }) => (
            <PlantCardPrimary
              data={item}
              onPress={() => handlePlantSelect(item)}
            />
          )}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          onEndReachedThreshold={0.1}
          onEndReached={({ distanceFromEnd }) =>
            handleFetchMore(distanceFromEnd)
          }
          ListFooterComponent={
            loadingMore && <ActivityIndicator color={colors.green} />
          }
        />
      </View>
    </SafeAreaView>
  );
};

export default PlantSelect;
