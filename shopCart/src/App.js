﻿import 'react-native-gesture-handler';
import { SENTRY_DSN } from '@env';
import React from 'react';
import './config/ReactotronConfig';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import * as Sentry from '@sentry/react-native';

import { store, persistor } from './store';

import Routes from './routes';

const App = () => {
  Sentry.init({
    dsn: SENTRY_DSN,
  });

  return (
    <NavigationContainer>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Routes />
        </PersistGate>
      </Provider>
    </NavigationContainer>
  );
};

export default App;
