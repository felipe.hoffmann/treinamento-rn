﻿import { all, takeLatest, call, put } from 'redux-saga/effects';
import api from '../../../services/api';
import { checkoutSuccess, checkoutFailure } from './actions';

export function* checkout({ payload }) {
  const { pedidos } = payload;

  const endpoint = '/api/mensagem';

  try {
    yield all(
      pedidos.map(pedido => {
        const body = {
          mensagem: pedido,
        };

        return call(api.put, endpoint, body);
      }),
    );

    yield put(checkoutSuccess());
  } catch (error) {
    yield put(checkoutFailure());
  }
}

export default all([takeLatest('@cart/CHECKOUT_REQUEST', checkout)]);
