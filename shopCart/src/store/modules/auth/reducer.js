﻿import produce from 'immer';

const INITIAL_STATE = {
  token: null,
  signed: false,
  loading: false,
  user: null,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@auth/SIGN_IN_FAILURE': {
        draft.signed = false;
        draft.token = null;
        draft.loading = false;
        draft.user = null;
        break;
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.signed = true;
        draft.loading = false;
        draft.token = action.payload.token;
        draft.user = action.payload.user;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.signed = false;
        draft.token = null;
        draft.loading = false;
        draft.user = null;
        break;
      }
    }
  });
}
