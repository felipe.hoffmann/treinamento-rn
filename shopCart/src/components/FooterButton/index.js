﻿import React from 'react';
import { ActivityIndicator } from 'react-native';

import { Container, Touchable, TextButton } from './styles';

const FooterButton = ({ text, onPress, disabled, loading }) => {
  return (
    <Touchable onPress={onPress} disabled={disabled}>
      <Container disabled={disabled}>
        {loading ? (
          <ActivityIndicator color="#fff" />
        ) : (
          <TextButton>{text}</TextButton>
        )}
      </Container>
    </Touchable>
  );
};

export default FooterButton;
