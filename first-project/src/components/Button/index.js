﻿import React from "react";
import { 
  StyleSheet, 
  TouchableOpacity,
  Text 
} from 'react-native';

const Button = ({ onPressButton, title = 'Valor default' }) => {  
  return (
    <TouchableOpacity
      style={styles.button}
      onPress={onPressButton}
    >
      <Text style={styles.textButton}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#fc7422',
    height: 56,
    justifyContent: 'center',
    borderRadius: 16,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
    elevation: 15
  },
  textButton: {
    color: '#fff',
    fontSize: 16,
  }
})

export default Button;