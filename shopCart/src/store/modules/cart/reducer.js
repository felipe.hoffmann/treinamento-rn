﻿import produce from 'immer';

const INITIAL_STATE = {
  pedidos: [],
  loading: false,
};

export default function cart(state = INITIAL_STATE, action) {
  return produce(state, draft => {
    switch (action.type) {
      case '@cart/ADD_TO_CART': {
        draft.pedidos.push(action.payload.pedido);
        break;
      }
      case '@cart/CHECKOUT_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@cart/CHECKOUT_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@cart/CHECKOUT_SUCCESS': {
        draft.loading = false;
        draft.pedidos = [];
        break;
      }
    }
  });
}
