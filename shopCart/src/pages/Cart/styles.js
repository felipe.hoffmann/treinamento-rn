﻿import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
`;

export const TextTitle = styled.Text`
  text-align: center;
  font-size: 20px;
  color: #333;
  padding: 10px;
`;

export const ContainerAmountControl = styled.View`
  flex-direction: row;
  border-radius: 16px;
  background: #fafafa;
  padding: 5px 10px;
  align-items: center;
  margin: 10px;
`;

export const TextItem = styled.Text`
  font-size: 20px;
  color: #333;
  margin-left: 10px;
`;

export const ButtonAddToCart = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  background: #fc7422;
  width: 80px;
  height: 80px;
  border-radius: 40px;
  align-self: center;
  margin-top: 10px;
`;

export const CheckoutButton = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  background: #fc7422;
  width: 80px;
  height: 80px;
  border-radius: 40px;
  align-self: center;
  margin-top: 10px;
`;

export const ContainerBtnCheckout = styled.View`
  padding: 8px 10px 8px 0;
  align-items: flex-end;
`;

export const ContainerIconBtnCheckout = styled.View`
  align-items: flex-end;
`;

export const IconQtdBtnCheckout = styled.View`
  background: #fff;
  border-radius: 16px;
  width: 20px;
  height: 20px;

  position: absolute;
  justify-content: center;
  align-items: center;
`;

export const TextQtdCheckout = styled.Text`
  font-size: 12px;
  font-weight: bold;
  color: #fc7422;
`;
