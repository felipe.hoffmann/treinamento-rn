﻿import styled from 'styled-components/native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export const Touchable = styled.TouchableOpacity`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const IconSignOut = styled(Icon).attrs({
  name: 'logout',
  size: 30,
})`
  color: #fff;
  margin: 0 10px;
`;
