﻿import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';
import PropTypes from 'prop-types';

import colors from '../../styles/colors';
import fonts from '../../styles/fonts';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.shape,
    width: 76,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 12,
    marginHorizontal: 5,
  },
  containerActive: {
    backgroundColor: colors.green_light,
  },
  text: {
    color: colors.heading,
    fontFamily: fonts.text,
  },
  textActive: {
    fontFamily: fonts.heading,
    color: colors.green_dark,
  },
});

const EnvironmentButton = ({ title, active, ...rest }) => (
  <RectButton
    style={[styles.container, active && styles.containerActive]}
    {...rest}
  >
    <Text style={[styles.text, active && styles.textActive]}>{title}</Text>
  </RectButton>
);

EnvironmentButton.propTypes = {
  title: PropTypes.string.isRequired,
  active: PropTypes.bool,
};

EnvironmentButton.defaultProps = {
  active: false,
};

export default EnvironmentButton;
